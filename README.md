---
title: "Rapport de stage de Master 2 SIAD 2020—2021, parcours DS, effectué chez BNP Paribas"
author: "par Kiril ISAKOV"
language: "fr"
output:
  odt_document:
    reference_odt: my-styles.odt
  html_document:
    df_print: paged
    toc: yes
  word_document:
    reference_docx: my-styles.docx
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
	echo = FALSE,
	message = FALSE,
	warning = FALSE
)
```

Remerciements :

* à Youcef, pour sa patience, confiance et pédagogie ; également, pour son mentorat extra-professionnel ;

* à Stéphane, pour son efficacité, réactivité et disponibilité, sa communication synthétique, efficace et sans langue de bois, ainsi que son attachement fort à diffuser les connaissances ;

* à Coralie, pour sa proactivité phénoménale en vue de m'équiper le plus rapidement possible de divers droits d'accès et pour d'autres petits riens comme l'étui à badge super pratique ;

* à Mehdi, pour sa sympathie et ses nombreuses bonnes idées d'améliorations fonctionnelles ;

* à Jules, de m'avoir recruté, accueilli et de s'être posé, en amont, la question du modèle du PC portable le plus adéquat eu égard à la nature de mes tâches de développeur ;

* à Hana, de m'avoir proposé de jeter un oeil au fichier `Hardcoded.py`, ce qui a déclenché l'idée de complètement réécrire celui-ci ;

* à toutes et tous les collègues vers qui j'ai pu me tourner en cas de besoin à tout moment, pour leur disponibilité et leur bienveillance.

# Introduction

Après une première partie consacrée à la présentation du projet et des besoins en compétences qui ont conduit à mon recrutement pour ce stage, puis une deuxième partie consacrée aux tâches et missions que j'ai accomplies et à l'évaluation de la valeur ajoutée de ma contribution, je ferai une conclusion où je tenterai d'effectuer une analyse objective des limites et difficultés structurelles du travail au quotidien.

# Contexte

Cette première partie sera consacrée à exposer la nature, les besoins et les enjeux de la mission qui m'a été confiée et pour laquelle j'ai été recruté en qualité de stagiaire au sein de l'équipe-projet COMET.

## Projet COMET

Le projet COMET (**CO**ntrol and **ME**asurement **T**ools) est né il y a environ deux ans à partir des besoins suivants :

* conférer aux outils informatiques des tâches extrêmement nombreuses, rébarbatives et sujettes à des erreurs humaines, à savoir automatiser l'identification d'anomalies dans un très grand volume de données (quantifiées en des millions d'opérations comptables et extra-comptables, alimentées quotidiennement) ;

* le besoin de représenter, sous forme d'un tableau de bord en ligne sous Tableau Software, confortable et convivial, 

  * les anomalies identifiées les plus récentes ainsi qu'un historique ;
  
  * divers indices et mesures statistiques de la performance (également connues sous l'acronyme KPI, pour **K**ey **P**erformance **I**ndices) ;

* permettre aux collaboateurs libérés des tâches automatisées de se concentrer sur des tâches davantage macroscopiques pour lesquelles les compétences humaines d'analyse et de synthèse pourraient apporter une véritable valeur ajoutée ;

Les utilisateurs ciblés par la solution COMET étant des analystes employés dans différentes entités du groupe (au Bénélux, en Italie, en Espagne...), le gain promis par COMET devait être très sensible. Cependant, dans un premier temps —et jusqu'à la première mise en production—, l'équipe-projet COMET n'avait pas bénéficié de nouvelles ressources humaines (notamment, a minima, d'un architecte données, un développeur Teradata et d'un développeur Python), et avait dû commencer à mettre au point une solution très « artisanale » comme elle le pouvait. Ce n'est qu'après la mise en production de COMET et les premiers bons retours des utilisateurs qu'a pu se dessiner la suite du cycle de vie du prototype.

## Sous-projet « Industrialisation »

Bien que fonctionnel, le prototype COMET n'était pas pour autant impeccable et nécessitait un remodelage profond et urgent. Pour l'anecdote, le prototype était constitué d'un unique script Python faisant près de 40 000 lignes et qui a été par la suite découpé en plusieurs fichiers, mais toujours trop gros pour être facilement maintenables.

À la suite de la mise en production, les charges ont été réévaluées et un certain budget a été débloqué pour financer le recrutement d'un strict minimum de nouvelles ressources humaines —un architecte données, puis, quelques mois plus tard, un développeur Python et un développeur Teradata— pour mener à bien un sous-projet baptisé « Industrialisation »^[Le sous-projet a reçu l'appellation « Industrialisation » que je mets entre guillemets car celle-ci n'a de signification qu'au sein de l'équipe-projet COMET mais pas à l'extérieur de celle-ci. En effet, bien que l'équipe-projet COMET, vue de l'extérieur, ait continué d'être *une* équipe, le fait est que la poursuite de deux chantiers en parallèle a créé le besoin de diviser l'équipe en deux groupes : l'un, constitué de membres recrutés plus récemment, devait s'occuper de l'« industrialisation » alors que l'autre, constitué de membres plus anciens, devait continuer de faire évoluer le « prototype » mis en production et d'assurer le « service client ».] et visant à 

* automatiser les nombreux aspects manuels du prototype COMET, notamment le fait qu'une intervention humaine est actuellement nécessaire pour lancer COMET au quotidien, aux alentours de 4 heures du matin ;

* lisser progressivement les nombreux aspects techniques « artisanaux » du prototype COMET qui étaient susceptibles de poser problème à moyen et long terme, notamment au niveau de la résilience et de la maintenance ; par exemple, le fait que COMET original utilisait plusieurs SGBD (Teradata, SQLite et d'autres) ;

* cultiver chez les membres de l'équipe un respect pour la standardisation et les bonnes pratiques en matière de la méthdologie Scrum mais aussi de la conception, du développement, des tests, etc.

## Compétences recherchées

Comme cela se fait par ailleurs beaucoup pour compléter les équipes en interne, l'équipe-projet COMET s'appuie sur des sous-traitants externes (à hauteur d'environ deux-tiers de l'effectif de l'équipe) ainsi que des alternants et des stagiaires, habituellement des étudiants en master 2. Actuellement, à part moi-même, l'équipe emploie un autre stagiaire, arrivé en avril comme moi, et un alternant. Les deux font le master MIAGE à l'université Paris Dauphine.

L'équipe souffrait (et souffre encore) de l'absence en son sein de développeurs confirmés, capables de traduire, en toute autonomie, un besoin en un outil informatique qui non seulement répond aux besoins mais qui est également *maintenable*. Contrairement à l'idée reçue chez les non informaticiens, développer quelque chose qui fonctionne est une condition nécessaire mais pas suffisante : encore faut-il que le code soit suffisamment bien écrit pour permettre, plus tard, des choses comme l'ajout rapide et indolore de nouvelles fonctionnalités ou encore la reprise et poursuite du travail par un autre développeur. Cela peut être rendu possible si l'on s'en tient à ce qu'on appelle « les bonnes pratiques » dont le respect permet toujours de gagner du temps plus tard. Malheureusement, bien souvent les décideurs de tous les secteurs ne voient pas l'ambition d'un développeur confirmé de s'en tenir aux bonnes pratiques comme un facteur incontournable de la réussite du projet, mais plutôt comme une lubie impertinente d'un développeur beaucoup trop perfectionniste et incapable d'évaluer correctement les priorités du projet.

Pour ma part, j'ai été recruté en tant que stagiaire développeur pour mes compétences

* en programmation en Python et Shell ;

* en travail en ligne de commande sous UNIX/Linux ;

* en gestion de version sous Git ;

* d'analyse ;

* de synthèse ;

Également,

* la capacité de lire le SQL et faire des requêtes ;

* le fait d'être sensibilisé aux enjeux de « la *data* » ;

Si je devais situer ma fonction parmi les métiers de « la *data* », ce serait celle d'ingénieur *data*, comme on peut le constater à travers les trois schémas ci-dessous.

```{r, out.width = "90%"}
knitr::include_graphics("https://i.ibb.co/kJsFhxg/data-science-careers-flowchart-large.jpg")
``` 

Source: [KDnuggets.com](https://www.kdnuggets.com/2021/05/data-scientist-data-engineer-data-careers-explained.html)

```{r, out.width = "90%"}
knitr::include_graphics("https://miro.medium.com/max/2400/1*jmk4Q2GAeUM_eqUtMh99oQ.png")
``` 

Source: [towardsdatascience.com](https://towardsdatascience.com/data-engineer-vs-data-scientist-bc8dab5ac124)

```{r, out.width = "90%"}
knitr::include_graphics("https://www.springboard.com/library/static/0801c99d24a2f92fa8b39ecda9a8c644/c8ad9/DEC-dataengineer-datasciencevsdataengineer.png")
``` 

Source: [springboard.com](https://www.springboard.com/library/data-engineering/how-to-become/)

# Réalisations personnelles et contributions au travail collectif

Dans cette partie je vais présenter mes contributions en indiquant

* le besoin de départ ;

* la nature de ma réalisation, la méthodologie utilisée et les raisons qui la justifient ;

* si pertinent, la valeur ajoutée de ma réalisation.

À noter que mes réalisations et contributions sont exclusivement techniques ; aucune n'en est fonctionnelle.

## En Python

### Réécrire le fichier `Hardcoded.py` du prototype COMET

Il s'agissait d'un fichier Python extrêmement simple de quelques dizaines de lignes de code mais contenant par ailleurs une bonne trentaine de requêtes SQL de plus de 200 lignes chacune, ce qui avait fait passer la taille du fichier à plus de 6 000 lignes. Lesdites requêtes SQL étaient découpées çà et là pour intégrer des variables de Python. Le rôle du fichier consistait à compléter les requêtes en incrustant celles-ci des valeurs des variables. Le fichier faisait partie du prototype COMET et était déjà versionné sous Git.

Lorsqu'on m'a proposé de travailler dessus, le besoin de départ était de faire évoluer certaines requêtes de ce fichier. Quelqu'un de l'équipe m'a proposé, pour commencer, d'y jeter un oeil et de lui dire ce que j'en pensais. J'ai analysé le fichier et ai fait un retour circonstancié en marge du besoin de départ, en disant que le fichier souffrait clairement d'une hypertrophie injustifiée et qu'il serait judicieux de lui administrer, d'une part, un « toilettage » rapide ; et d'autre part, une réécriture de fond :

* Le toilettage rapide devait consister à se débarrasser de quelques lourdeurs assez classiques en programmation^[Par exemple, d'une pile de `if... elif... elif... else` faite d'une trentaine de conditions et susceptible de s'agrandir régulièrement. Il s'agit d'un cas d'école [bien connu dans le folklore d'internet](https://twitter.com/ctrlshifti/status/1288745146759000064) et il existe, en Python, [une solution préconisée par la documention](https://docs.python.org/fr/3/faq/design.html#why-isn-t-there-a-switch-or-case-statement-in-python)], remarquées çà et là dans le code du fichier ;

* La réécriture de fond, quant à elle, devait consister à faire sortir toutes les requêtes SQL du fichier Python et les placer dans des fichiers distincts. Cette opération permettrait de faire la paix avec plusieurs bonnes pratiques (et qui dit « bonnes pratiques », dit « moins de bugs » et « gain de temps ») telles que

  * réduire drastiquement la taille du fichier et donc faciliter sa maintenance (et donc moins de bugs et donc un gain de temps) ;

  * pratiquer ce qu'on appelle le « couplage faible » (à chaque nouvelle évolution ne modifier que le fichier concerné) ;
  
  * se débarrasser des variables globales, une autre mauvaise pratique car elle empêche d'établir la précédence ; la seule exception pouvant être faite pour les constantes, mais qu'il convient dès lors de déclarer comme telles ;
  
  * se débarrasser du code dupliqué (lorsqu'il y a du code dupliqué, chaque nouvelle évolution doit être implémentée à autant d'endroits qu'il y a de doublons) ; etc.^[Pour en savoir plus sur les bonnes pratiques, cf [Annexe A](#annexe-a-bonnes-pratiques-en-matière-de-développement) en fin du rapport]

Assez réticents au début, les membres de l'équipe m'ont finalement laissé ma chance de réécrire le fichier sur une branche Git indépendante (n'ayant pas d'incidence sur le prototype déployé en production). Des tests unitaires ont montré que le code réécrit produisait exactement le même résultat, à ceci près qu'il n'y avait plus un fichier de 6 000 lignes mais seulement d'une centaine de lignes, plus une trentaine de fichiers SQL proprement formatés, plus lisibles (donc plus facilement maintenables, donc gain de temps).^[Pour plus de détails au sujet de la réécriture du fichier `Hardcoded.py`, cf [Annexe B](#annexe-b-le-principe-de-la-réécriture-du-fichier-hardcoded.py) en fin du rapport] Une proposition finalement bien tombée, bien accueillie et qui, j'espère, servira d'exemple pour réduire la complexité des autres fichiers Python volumineux.

### Réécrire et terminer le script utilitaire de génération de SQL

Soit un fichier Excel qui représente une source d'information extrêmement précieuse pour le projet car il contient des informations pour la création de toutes les tables Teradata du projet : on parle plusieurs milliers de tables et leurs quelque 30 000 champs.

Soit également un script en Python qui devait être lancé manuellement pour lire les informations contenues dans ledit fichier Excel et générer à partir de celles-ci des requêtes SQL qu'il sauvegardait ensuite dans des fichiers SQL. Il s'agissait de requêtes SQL de création de tables et le script était censé générer autant de requêtes de création qu'il y avait de tables à créer.

Bien qu'assez abouti et même doté d'une petite fenêtre d'interface, le script n'était pas terminé car il ne remplissait pas tout à fait ce pourquoi il avait été créé : déjà à même de générer du SQL pour la création d'une seule table ($1$ table) et de toutes les tables ($n$ tables), il ne savait néanmoins pas encore en faire de même pour une sélection quelconque de tables (un nombre $x \Leftrightarrow 1 \leqslant x \leqslant n $). Il s'agissait pour moi, au départ, d'ajouter au script uniquement cette fonctionnalité-là.

Une fois cela fait, d'autres anomalies ont fait leur apparition^[Exemples : Dans le bloc CREATE, la dernière ligne (autrement dit la ligne qui cite le dernier champ) ne doit pas se terminer par une virgule. Le bloc `PRIMARY INDEX` ne peut citer que des champs marqués comme « PI ». Un champ marqué `PRIMARY INDEX` ne peut pas être `COMPRESS` (et inversement).], si bien que j'ai fini par me dire que cela valait la peine de tout réécrire proprement :

* supprimer le code dupliqué ;

* supprimer les variables globales et déclarer correctement les constantes ;

* abandon de l'interface graphique au profit de l'interface en ligne de commande, et ce pour plusieurs raisons:
  
  * possibilité de voir dans le terminal les traces d'exécution et surtout d'erreurs ;
  
  * possibilité de se concentrer sur les fonctionnalités essentielles sans se préoccuper des aspects cosmétiques ;
  
  * possibilité de définir de manière transparente divers paramètres au moment de lancer l'exécution (indiquer l'emplacement du fichier Excel, indiquer l'emplacement du répertoire prévu pour les fichiers générés, indiquer l'encodage souhaité des fichiers générés, etc.) ;
  
  * possibilité, dès lors, d'automatiser le lancement du programme par d'autres programmes (typiquement, un ordonnanceur mais aussi tout autre programme compatible avec l'interface en ligne de commande)

L'interface en ligne de commande étant comme une lunette qui offre une vue du « squelette » d'un programme, il est toujours préférable de commencer par créer son programme en version prévue pour l'interface en ligne de commande. Et il est plus facile d'« habiller » le programme d'une interface graphique par la suite que de faire en sorte que l'interface graphique affiche les traces d'exécution et les messages d'erreur.

Au fur et à mesure, j'ajoutais d'autres fonctionnalités, parfois de moi et parfois suggérées par les confrères :

* gérer le développement dudit script sous Git afin de pouvoir garder un historique et profiter de la puissance et de la flexibilité de Git ;

* une présentation plus soignée du code SQL généré (par exemple, une légère indentation à l'intérieur de blocs de code, pour faciliter la lecture) ;

* nettoyage de certaines données avant de les insérer dans du code SQL à générer (typiquement, remplacer des caractères non compatibles avec Teradata par leurs équivalents compatibles : par exemple, remplacer `'` par `''` ainsi que des caractères Unicode par des caractères ASCII) ;

* possibilité d'avoir non pas une liste de noms de tables à générer mais plusieurs listes (un peu comme si on avait différents « profils » avec différents nom de tables à générer)

## En Shell

### Réécrire et terminer le mécanisme de lancement de COMET « industrialisé »

Soit un ensemble de scripts Korn Shell (KSH) destinés à lancer COMET sur le serveur applicatif, accompagner son exécution en générant des traces de journalisation et d'erreur qui doivent à la fois s'afficher en console et également être sauvegardées dans un fichier `.log`.

Il s'agissait de terminer le développement déjà fait en grande partie, de le compléter, corriger, apprêter, standardiser et harmoniser un petit peu l'ensemble (par exemple, le format des traces de journalisation), supprimer tout le code dupliqué, limiter l'usage de variables globales, etc. Un sujet pour lequel il y avait beaucoup à faire mais finalement peu à dire dans le présent rapport. Une mission accomplie mais qui a néanmoins apporté son lot de frustration et d'apathie face à toutes les difficultés structurelles empêchant un informaticien de faire son travail dans de bonnes conditions :

* sans avoir accès au serveur et étant par conséquent obligé de s'évertuer pour tester mon code à l'aide de l'un ou l'autre collègue qui possède l'accès au serveur ;

* sous la pression pesant au quotidien sur l'équipe pour accélérer le travail et effectuer la mise en production ;

* chercher à finaliser un livrable de code « industrialisé » complet alors même que le prototype COMET continue de se développer en parallèle, ce qui a également généré un certain nombre de complications pour l'équipe.

Cependant, ce qui m'a permis de mener à bien ce travail malgré les difficultés organisationnelles et structurelles, c'est 

* la confiance de Youcef, le responsable du sous-projet « Industrialisation », ce qui m'a permis d'avoir davantage de marge de manoeuvre ;

* le fait d'être entouré de collègues compétents et disponibles, vers qui j'ai pu me tourner en cas de besoin à tout moment ; et j'espère leur avoir renvoyé l'ascenseur en me rendant, à mon tour, également utile et disponible à leur égard ;

## Autres tâches et missions

Pour finir, quelques tâches techniques mais sans lien direct avec la programmation.

### Participation à la rédaction du dossier d'installation

Il faut savoir que les normes de sécurité chez BNP Paribas ne permettent normalement pas aux membres d'équipes-projets^[Dans une organisation DevOps, on parlerait d'équipes du type Dev] d'accéder directement aux serveurs applicatifs de niveau « qualification » ou « production ». Ces opérations sont accomplies par d'autres équipes, transversales,^[Dans une organisation DevOps, on parlerait d'équipes du type Ops] qui ont l'accès à la production mais en revanche ignorent la nature et la portée de ce qu'ils manipulent. Pour cette raison, les équipes-projets fournissent à ces derniers un certain nombre de documents formalisés, censés les guider.

Un dossier d'installation est un document Word très formalisé, avec un grand nombre de sections et sous-sections qu'il s'agit de remplir avec des informations et instructions destinées à guider l'équipe transversale dans l'installation (on dit aussi le déploiement) des fichiers d'un livrable finalisé sur un serveur applicatif de niveau « qualification » ou « production ». La qualité de l'information fournie est déterminante dans la mise en service du livrable : les instructions doivent être claires, précises et synthétiques ; la mise en page doit faciliter la lecture et pas l'obscurcir. Il s'agissait pour moi de remplir certaines sections de ce document en prenant soin de remplir les bonnes « cases » avec les bonnes informations et de rendre ces dernières parfaitement lisibles (typiquement, saisir des mots techniques —commandes, identifiants, mots de passe, URLs— à l'aide d'une [police d'écriture à chasse fixe](https://www.wikiwand.com/fr/Police_d'écriture_à_chasse_fixe) et non du Calibri ou du Arial, choisies par défaut).

Étant donné que j'attache une grande attention à la typographie, sans pour autant que j'aime m'épuiser inutilement, il y a un certain temps que j'ai pris l'habitude de préparer mes textes à l'aide de technologies plus ergonomiques que Microsoft Word et convertir (ou coller) ceux-ci dans Word à la toute fin. J'utilise pour cela Markdown, R Markdown, $\LaTeX$ et LibreOffice.^[La différence de confort ne se fait pas forcément ressentir dès la première « itération » mais en revanche à partir de la deuxième, troisième itérations et ainsi de suite, lorsque je suis amené à apporter des modifications au texte que je dois fournir.]

### Paramétrage des manipulations automatiques commandées au progiciel ARA

Il s'agissait pour nous de pouvoir valoriser un certain nombre de variables immédiatement après le déploiement de l'application COMET « industrialisée », en fonction de l'environnement où celle-ci est déployée. La valorisation doit se faire de manière automatisée, à l'aide d'un progiciel appelé ARA (Application Release Automation).

Cette tâche doit être préparée en tandem avec l'équipe du progiciel ARA 

* à qui nous devons fournir les instructions sous forme d'un fichier XML où sont codifiés les informations relatives aux environnements, fichiers, noms de variables qui s'y trouvent et les valeurs attendues ;

* et en laissant des marques spécifiques dans le contenu des fichiers d'intérêt, précisément à des endroits dans le code que nous souhaitons voir être valorisés.

L'équipe ARA nous fournit en échange la documentation avec des explications et des exemples. Les échanges directs avec les personnes de leur équipe sont limités.

Même si le sujet était nouveau pour moi, Je me sentais assez à l'aise dans l'élaboration du fichier XML en question car j'en connaissais néanmoins déjà les principes de la configuration de logiciel à l'aide de fichiers XML, auxquels j'avais déjà été rompu par le passé, lorsque j'étais développeur Java EE en ESN. J'ai réussi sinon du premier coup, un seul aller-retour par mail a suffi pour apporter une petite correction et le fichier était prêt.

# Conclusion

Dans cette dernière partie je vais énumérer les limites structurelles de l'organisation et autres difficultés rencontrées au quotidien, tenter d'en établir les causes et envisager des solutions :

* limites au niveau de l'organisation du groupe et de la politique des ressources humaines (RH) : 

  * les RH réduites et la réticence du groupe à accorder davantage de budget pour élargir les RH de la DSI entraînent

    * des pertes de connaissances dues à l'emploi de très nombreux prestataires externes dont la fin (ou le changement) de mission est régulièrement mal anticipée, même avec toute la bonne volonté du monde ;

    * une surcharge de travail pour les équipes transversales 

      * dont les membres ne peuvent pas prêter une quantité suffisante d'attention à toutes les demandes qu'elles et ils sont amenés à devoir traiter, 

      * d'où des difficultés endémiques de communication inter-équipe et inter-services, 
      * d'où des retards et des oublis fréquents dans la prise en charge des demandes ;

    * une surcharge de travail pour les équipes-projets que le grand volume de travail oblige à

      * sauter les étapes pourtant incontournables d'un projet informatique comme la rédaction de spécifications fonctionnelles et techniques préalables au début du développement ;

      * prendre en charge des tâches qui ne relèvent pas de leur spécialité, comme la rédaction de la documentation en anglais, sans personne pour les relire, ce qui fait que la documentation souffre de nombreuses imprécisions lexicales (voire probablement de la possibilité de faire faux-sens ou contresens) ;

      * n'exploiter que des fonctionnalités de base d'outils pourtant puissants comme le SGBD Teradata ;

* limites au niveau de la sécurité de la DSI du groupe : les mesures de sécurité sont souvent extrêmement contraignantes et

  * sont vécus péniblement par les collaborateurs de la DSI, alors que, bien souvent, les outils alternatifs soit n'existent pas au sein du groupe, soit existent mais sont inconnus des collaborateurs ;

  * obligent parfois les collaborateurs à recourir à des pratiques détournées pour accomplir leurs tâches (passer par du « shadow IT » ; partager un token SecurID entre plusieurs personnes ; envoyer le contenu d'un document par mail à défaut de pouvoir envoyer un fichier ; etc.)

* limites au niveau de l'utilisation des outils : 

  * une responsabilité trop grande est conférée à des fichiers Excel dont la manipulation collective (comme si ceux-ci étaient des bases de données fiables) provoque fréquemment des pertes ou l'altération de l'information, à moins de faire pratiquer des contre-mesures (conserver des copies de sauvegarde tous les jours ou presque, etc.), qui représentent du travail supplémentaire ;


# Annexe A : Bonnes pratiques en matière de développement

Lire sur [gitlab.com/kirisakow/bonnes-pratiques-de-developpement](https://gitlab.com/kirisakow/bonnes-pratiques-de-developpement)

# Annexe B : Le principe de la réécriture du fichier `Hardcoded.py`

## Le couplage faible : le principe

Le code Python et les templates SQL devraient être conservés dans des fichiers séparés, capables toutefois de lire l'un l'autre, comme ceci :

![](img/principe_couplage_faible_python_sql.png)


## Avant la réécriture :

Un fichier Python contenant du SQL en dur :

```python
def RGCEC024(entite):
    Part1 = str("""INSERT INTO """ + prefixe_work + """.MDC_POC_ANO_TEST_A
(...)
        FROM
            """ + prefixe_stg + """.V_ECHEANCIER_C_""" + entite + """ a
        INNER JOIN """ + prefixe_work + """.mdc_sar b
(...)
    """)
    return Part1
```

## À présent :

Un fichier SQL avec des variables Python incrustées dedans :

```sql
INSERT INTO {PREFIX_DB['WORK']}.MDC_POC_ANO_TEST_A
(...)
        FROM
            {PREFIX_DB['STG']}.V_ECHEANCIER_C_{entite} a
        INNER JOIN {PREFIX_DB['WORK']}.mdc_sar b
(...)
```

Un fichier Python qui retrouve et lit le fichier SQL et valorise les variables incrustées dedans :

```python
from constants import PREFIX_DB

def RGCEC024(entite):
    path_to_sql_template = 'RGCEC024.sql'
    template_content_raw = ''
    try:
        with open(file=path_to_sql_template, mode='r') as f:
            template_content_raw = f.read()
    except Exception as e:
        print(f"Error: SQL template '{path_to_sql_template}' not found")
        print()
        print(str(e))
    try:
        Part1 = eval('f"""' + template_content_raw + '"""')
        return Part1
    except Exception as e:
        print(f"An error occurred while trying to fill the SQL template '{path_to_sql_template}' with parameters")
        print()
        print(str(e))
```

Bien que l’extension de ces nouveaux fichiers « templates SQL » soit `.SQL`, leur contenu n’est pas du SQL au sens strict car il contient aussi des noms des variables Python (notez leur présence entre accolades : `{entite}` ; `{PREFIX_DB['STG']}` ; `{PREFIX_DB['WORK']}`)

Pour assurer l'interconnexion entre les scripts Python et ces nouveaux fichiers « templates SQL », les noms des variables Python que ces derniers contiennent doivent être préservés en l'état lors de chaque nouvelle mise à jour d’un des « templates SQL ».