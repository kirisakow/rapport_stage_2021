## Le couplage faible : le principe

Le code Python et les templates SQL devraient être conservés dans des fichiers séparés, capables toutefois de lire l'un l'autre, comme ceci :

![](img/principe_couplage_faible_python_sql.png)


## Avant la réécriture :

Un fichier Python contenant du SQL en dur :

```python
def RGCEC024(entite):
    Part1 = str("""INSERT INTO """ + prefixe_work + """.MDC_POC_ANO_TEST_A
(...)
        FROM
            """ + prefixe_stg + """.V_ECHEANCIER_C_""" + entite + """ a
        INNER JOIN """ + prefixe_work + """.mdc_sar b
(...)
    """)
    return Part1
```

## À présent :

Un fichier SQL avec des variables Python incrustées dedans :

```sql
INSERT INTO {PREFIX_DB['WORK']}.MDC_POC_ANO_TEST_A
(...)
        FROM
            {PREFIX_DB['STG']}.V_ECHEANCIER_C_{entite} a
        INNER JOIN {PREFIX_DB['WORK']}.mdc_sar b
(...)
```

Un fichier Python qui retrouve et lit le fichier SQL et valorise les variables incrustées dedans :

```python
from constants import PREFIX_DB

def RGCEC024(entite):
    path_to_sql_template = 'RGCEC024.sql'
    template_content_raw = ''
    try:
        with open(file=path_to_sql_template, mode='r') as f:
            template_content_raw = f.read()
    except Exception as e:
        print(f"Error: SQL template '{path_to_sql_template}' not found")
        print()
        print(str(e))
    try:
        Part1 = eval('f"""' + template_content_raw + '"""')
        return Part1
    except Exception as e:
        print(f"An error occurred while trying to fill the SQL template '{path_to_sql_template}' with parameters")
        print()
        print(str(e))
```

Bien que l’extension de ces nouveaux fichiers « templates SQL » soit `.SQL`, leur contenu n’est pas du SQL au sens strict car il contient aussi des noms des variables Python (notez leur présence entre accolades : `{entite}` ; `{PREFIX_DB['STG']}` ; `{PREFIX_DB['WORK']}`)

Pour assurer l'interconnexion entre les scripts Python et ces nouveaux fichiers « templates SQL », les noms des variables Python que ces derniers contiennent doivent être préservés en l'état lors de chaque nouvelle mise à jour d’un des « templates SQL ».